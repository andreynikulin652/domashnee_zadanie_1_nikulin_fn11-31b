#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <map>
#include <set>
#include <cstring>

using namespace std;

struct person
{
	char name[20];
	char district [30];
	long int telephone;
	long int dogovor;
	char data[20];
	int price_of_install;
	int price;
	char data_of_pay[20];
	friend std::ostream& operator <<(std::ostream&, const person&);
	friend std::istream& operator >>(std::istream&, person&);
	friend bool operator <(const person&, const person&);
	friend bool operator >(const person&, const person&);
	friend bool operator ==(const person&, const person&);
	friend bool operator !=(const person&, const person&);
	person(const person&);
	person();
};

class Person_data
{
private:
	std::string name_of_file;
	map<long int, person> load_from_file();
public:
	Person_data(const std::string& name);
	Person_data();
	void add(const person&);
	void insert(const person&);
	void delete_person(long int);
	void change_person(long int, const person&);
	void find_by_district(const std::string&, const string&);
	void find_by_date(const std::string&, const string&);
	void find_by_date_of_contract(const std::string&, const string&);
	
};

void from_bin_to_txt(const std::string&, const std::string&);

