#pragma once
#include <vector>
#include <list>
#include <fstream>
#include <iterator>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <exception>
#include <functional>
#include <vector>

void create_Data(const std::string& filename);
void create_Data_2(const std::string& filename);

using namespace std;

template<typename container>
container load_data(const std::string& name)
{
	std::ifstream fout(name);
	if (!fout.is_open())
		throw std::exception("File hasn't opened");
	container buf;
	std::back_insert_iterator<container> p_iter(buf);

	while (!fout.eof())
	{
		int temp;
		fout >> temp;
		p_iter = temp;
	}
	fout.close();
	return buf;
}

template<typename container>
container load_data_2(const std::string& name)
{
	std::ifstream fout("a.txt");
	if (!fout.is_open())
		throw std::exception("File hasn't opened");
	
	container buf;
	std::back_insert_iterator<container> p_iter(buf);
	copy(std::istream_iterator<int>(fout), std::istream_iterator<int>(), p_iter);
	fout.close();
	return buf;
}

template<typename container>
void modify(container& c)
{
	auto found = find_if(c.begin(), c.end(), [](int t)
		{
			return t % 2 != 0;
		});
	int t = abs((*found));
	int summ = 0;
	int i = 0;
	for_each(c.begin(), c.end(), [&summ, &i](int x)
		{
			summ = summ + abs(x);
			i++;
		});
	for_each(c.begin(), c.end(), [t](int x)
		{
			x = x + t;
		});
	c.push_back(summ);
	c.push_back(summ / i);
}

template<typename iterator>
void modify(const iterator& begin, const iterator& end)
{
	auto found = find_if(begin, end, [](int x) {
		return x % 2 != 0;
		});
	int t = abs((*found));
	for_each(begin, end, [t](int& x)
		{
			x = x + t;
		});
}

template < typename Container>
void modify(Container& v, const  std::function<void(int&)>& f)
{
	std::for_each(v.begin(), v.end(), f);
}


void Modify(const std::string& path, const std::string& d_path);


template<typename container>
void output_result(const container& con, const std::string& str)
{
	std::ofstream fout(str);
	for_each(con.begin(), con.end(), [&fout](int x)
		{
			fout << x;
		});
}

template<typename container>
void output_result_1(container& con, const std::string& str)
{
	std::ofstream fout(str);
	copy(con.begin(), con.end(), std::ostream_iterator<int>(fout));
}