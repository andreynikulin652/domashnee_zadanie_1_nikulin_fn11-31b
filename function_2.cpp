#include "function_2.h"

using namespace std;

std::ostream& operator <<(std::ostream& out, const person& a)
{
	out << setw(30) << left << a.name
		<< setw(30) << left << a.district
		<< setw(16) << left << a.telephone
		<< setw(20) << left << a.dogovor
		<< setw(16) << left << a.data
		<< setw(10) << left << a.price_of_install
		<< setw(10) << left << a.price
		<< setw(16) << left << a.data_of_pay;
	return out;
}



std::istream& operator >>(std::istream& in, person& a)
{
	in >> a.name;
	in >> a.district;
	in >> a.telephone;
	in >> a.dogovor;
	in >> a.data;
	in >> a.price_of_install;
	in >> a.price;
	in >> a.data_of_pay;
	return in;
}

person::person(const person& a)
{
	strcpy_s(this->data, a.data);
	strcpy_s(this->district, a.district);
	strcpy_s(this->data_of_pay, a.data_of_pay);
	strcpy_s(this->name, a.name);
	this->dogovor = a.dogovor;
	this->price = a.price;
	this->price_of_install = a.price_of_install;
	this->telephone = a.telephone;
}
person::person()
{

}

Person_data::Person_data(const string& name)
{
	this->name_of_file = name;
}

bool operator <(const person& one, const person& two)
{
	return one.dogovor < two.dogovor;
}

bool operator >(const person& one, const person& two)
{
	return one.dogovor > two.dogovor;
}

bool operator ==(const person& one, const person& two)
{
	return one.dogovor == two.dogovor;
}

bool operator != (const person& one, const person& two)
{
	return one.dogovor != two.dogovor;
}
Person_data::Person_data()
{

}

void Person_data::add(const person& a)
{
	std::ofstream file(this->name_of_file, std::ios_base::app | std::ios_base::binary | std::ios_base::out);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	file.write((char*)&a, sizeof(a));
	file.close();
}

void Person_data::insert(const person& a)
{
	std::ofstream file(this->name_of_file, std::ios::binary | ios::app);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	file.write((char*)&a, sizeof(a));
	file.close();
}

map<long int, person> Person_data::load_from_file()
{
	ifstream file(this->name_of_file, ios::binary | ios::in);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	map<long int, person> a;
	person buf;
	while (file.read((char*)&buf, sizeof(buf)))
	{
		a.insert(make_pair(buf.dogovor, buf));
	}
	file.close();
	return a;
}

void Person_data::delete_person(long int num)
{
	map<long int, person> buf = load_from_file();
	std::ofstream file(this->name_of_file, std::ios_base::binary | ios_base::trunc|ios_base::out);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	map<long int, person>::iterator it = buf.begin();
	while (it != buf.end())
	{
		if (it->first != num)
		{
			person p(it->second);
			file.write((char*)&p, sizeof(p));
		}
		++it;
	}
	file.close();
}

void Person_data::change_person(long int changable_person, const person& new_person)
{
	map<long int, person> buf = load_from_file();
	std::ofstream file(this->name_of_file, std::ios_base::binary | ios_base::trunc | ios_base::out);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	map<long int, person>::iterator it = buf.begin();
	while (it != buf.end())
	{
		if (it->first == changable_person)
		{
			person p(new_person);
			file.write((char*)&p, sizeof(p));
		}
		else
		{
			person p(it->second);
			file.write((char*)&p, sizeof(p));
		}

		++it;
	}
	file.close();
}

void Person_data::find_by_district(const string& name_of_new_file, const string& needed_district)
{
	multimap<string, person> buf;
	ifstream file(this->name_of_file, ios::binary | ios::in);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	person a;
	while (file.read((char*)&a, sizeof(a)))
	{
		buf.insert(make_pair(a.district, a));
	}
	file.close();
	ofstream fout(name_of_new_file, ios::binary | ios::out);
	if (!fout.is_open())
	{
		throw std::exception("file error");
	}
	multimap<string, person>::iterator beg = buf.lower_bound(needed_district);
	auto end = buf.upper_bound(needed_district);
	while (beg != end)
	{
		person p(beg->second);
		fout.write((char*)&p, sizeof(p));
		++beg;
	}
	fout.close();
}

void Person_data::find_by_date(const string& name_of_new_file, const string& needed_date)
{
	multimap<string, person> buf;
	ifstream file(this->name_of_file, ios::binary | ios::in);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	person a;
	while (file.read((char*)&a, sizeof(a)))
	{
		buf.insert(make_pair(a.data, a));
	}
	file.close();
	ofstream fout(name_of_new_file, ios::binary | ios::out);
	if (!fout.is_open())
	{
		throw std::exception("file error");
	}
	multimap<string, person>::iterator beg = buf.lower_bound(needed_date);
	auto end = buf.upper_bound(needed_date);
	while (beg != end)
	{
		person p(beg->second);
		fout.write((char*)&p, sizeof(p));
		++beg;
	}
	fout.close();
}

void Person_data::find_by_date_of_contract(const string& name_of_new_file, const string& needed_date)
{
	multimap<string, person> buf;
	ifstream file(this->name_of_file, ios::binary | ios::in);
	if (!file.is_open())
	{
		throw std::exception("file error");
	}
	person a;
	while (file.read((char*)&a, sizeof(a)))
	{
		buf.insert(make_pair(a.data, a));
	}
	file.close();
	ofstream fout(name_of_new_file, ios::binary | ios::out);
	if (!fout.is_open())
	{
		throw std::exception("file error");
	}
	multimap<string, person>::iterator beg = buf.lower_bound(needed_date);
	auto end = buf.upper_bound(needed_date);
	while (beg != end)
	{
		person p(beg->second);
		fout.write((char*)&p, sizeof(p));
		++beg;
	}
	fout.close();
}

void from_bin_to_txt(const std::string& bin_name, const std::string& txt_name)
{
	std::ifstream bin_file(bin_name, std::ios_base::binary | std::ios_base::in);
	if (!bin_file.is_open())
	{
		throw std::exception("file error");
	}

	std::ofstream txt_file(txt_name, std::ios_base::binary | std::ios_base::out);
	if (!txt_file.is_open())
	{
		throw std::exception("file error");
	}

	txt_file << std::setw(20) << std::left << "Name:"
		<< std::setw(15) << std::left << "District:"
		<< std::setw(20) << std::left << "telephone:"
		<< std::setw(20) << std::left << "dogovor:"
		<< std::setw(20) << std::left << "data:"
		<< std::setw(20) << std::left << "price of install:"
		<< std::setw(25) << std::left << "price:"
		<< std::setw(25) << std::left << "data of pay:" << std::endl;



	person client;
	while (bin_file.read((char*)&client, sizeof(client)))
	{


		txt_file << client << std::endl;
	}

	txt_file.close();
	bin_file.close();

}
