#include "functions.h"
#include <ctime>
#include <cstdlib>

using namespace std;

void create_Data(const std::string& filename)
{
	srand(time(NULL));
	int a;
	std::ofstream fout (filename);
	if (!fout.is_open())
		throw exception("File hasn't opened");
	for (int i = 0; i < 100; i++)
	{
		a = rand() % 100 - 50;
		fout << a;
		fout << "\n";
	}
	fout.close();
}

void create_Data_2(const std::string& filename)
{
	srand(time(NULL));
	vector<int> buf(100);
	generate(buf.begin(), buf.end(), []()
		{
			return rand() % 100 - 50;
		});
	std::ofstream fout(filename);
	if (!fout.is_open())
		throw exception("File hasn't opened");
	ostream_iterator<int> it(fout, "\n");
	copy(buf.begin(), buf.end(), it);
	fout.close();
}

void Modify(const std::string& path, const std::string& d_path)
{

	std::ifstream f(path, std::ios_base::in);

	std::vector<int> v;
	v = load_data<std::vector<int>>(path);
	auto t = find_if(v.begin(), v.end(), [](int a)
		{
			return a % 2 != 0;
		});
	int a = *t;
	a = abs(a);
	std::ofstream res(d_path);

	std::transform(std::istream_iterator<int>(f), std::istream_iterator<int>(), std::ostream_iterator<int>(res, " "), [&a](int value)
		{
			return value + a;
		});

	f.close();
	res.close();
}


