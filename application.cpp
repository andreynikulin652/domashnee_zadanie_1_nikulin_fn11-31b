#include "application.h"

void Application1::run_fileModify()
{
	std::cout << "Write name of file from what one you want to read" << std::endl;
	std::cin >> path;
	std::cout << "Write name of file in what one you want to input result" << std::endl;
	std::string d_path;
	std::cin >> d_path;
	Modify(path, d_path);
}

void Application1::run_CreateData()
{
	std::cout << std::setw(30) << std::left << "1. Create Data by file stream"
		<< std::setw(30) << std::right << "2. Create Data by std::copy" << std::endl;

	std::cin >> choice;
	std::cout << "Write name of file where you want to create data" << std::endl;
	std::cin >> path;

	switch (choice)
	{
	case 1:
		create_Data(path);
		break;
	case 2:
		create_Data_2(path);
	default:
		break;
	}

}

void Application1::run_LoadData()
{
	std::cout << std::setw(30) << std::left << "1. Load Data by file stream "
		<< std::setw(30) << std::right << "2. Load Data by std::copy" << std::endl;

	std::cin >> choice;
	std::cout << "Write name of file from which one you want to load data " << std::endl;
	std::cin >> path;
	switch (choice)
	{
	case 1:
		v = load_data<std::vector<int>>(path);
		break;
	case 2:
		v = load_data_2<std::vector<int>>(path);
	default:
		break;
	}
}

void Application1::run_Modify() {

	std::cout << "1. Modify: push back abs_sum, abs_avg and increase all elements on the first odd element" << std::endl
		<< "2. Modify: increase all elements on the first odd element (parametrs are iterators)" << std::endl
		<< "3. Modify: another operation on all elements by lymbda" << std::endl
		<< "4. Modify: from one file to another and increase all elements on the first odd element " << std::endl;

	std::cin >> choice;
	switch (choice)
	{
	case 1:
		modify<std::vector<int>>(v);
		break;
	case 2:
		modify(v.begin(), v.end());
		break;
	case 3:
		modify<std::vector<int>>(v, [](int& value)
			{
				value *= 2;
			});
		break;
	case 4:
		run_fileModify();
	default:
		break;
	}

};

void Application1::run_InputResult()
{
	std::cout << std::setw(30) << std::left << "1. IntputResult by file stream "
		<< std::setw(30) << std::right << "2. InputResult by std::copy" << std::endl;

	std::cin >> choice;
	std::cout << "Write name of file where you want to output result" << std::endl;
	std::cin >> path;
	switch (choice)
	{
	case 1:
		output_result<std::vector<int>>(v,path);
		break;
	case 2:
		output_result_1<std::vector<int>>(v,path);
	default:
		break;
	}
}

void Application1::run()
{
	std::cout << std::setw(50) << std::right << "FIRST TASK:" << std::endl;
	std::cout << "MENU" << std::endl
		<< "1. CreateData" << std::endl
		<< "2. LoadData" << std::endl
		<< "3. Modify" << std::endl
		<< "4. InputResult" << std::endl
		<< "Press another button to exit" << std::endl;

	bool key = true;

	while (key)
	{
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			run_CreateData();
			break;
		case 2:
			run_LoadData();
			break;
		case 3:
			run_Modify();
			break;
		case 4:
			run_InputResult();
			break;
		default:
			key = false;
			break;
		}
	}
}

Application2::Application2(const std::string& _path)
{
	path = _path;
	data = Person_data(_path);
}

void Application2::run_add()
{
	person client;

	std::cout << "Input client to add" << std::endl;

	std::cout << "Clients fields " << '\n';
	std::cout << std::setw(20) << std::left << "Name:"
		<< std::setw(15) << std::left << "District:"
		<< std::setw(20) << std::left << "telephone:"
		<< std::setw(20) << std::left << "dogovor:"
		<< std::setw(20) << std::left << "data:"
		<< std::setw(20) << std::left << "price of install:"
		<< std::setw(25) << std::left << "price:"
		<< std::setw(25) << std::left << "data of pay:" << std::endl;
	std::cin >> client;
	data.add(client);
}

void Application2::run_insert()
{
	person client;
	std::cout << "Input client to insert" << std::endl;
	std::cout << "Clients fields " << '\n';
	std::cout << std::setw(20) << std::left << "Name:"
		<< std::setw(15) << std::left << "District:"
		<< std::setw(20) << std::left << "telephone:"
		<< std::setw(20) << std::left << "dogovor:"
		<< std::setw(20) << std::left << "data:"
		<< std::setw(20) << std::left << "price of install:"
		<< std::setw(25) << std::left << "price:"
		<< std::setw(25) << std::left << "data of pay:" << std::endl;
	std::cin >> client;
	data.insert(client);
}

void Application2::run_erase()
{
	std::cout << "Input client's dogovor to erase" << std::endl; 
	long int num; 
	cin >> num;
	data.delete_person(num);
}

void Application2::run_change()
{
	person client;
	long int num;
	std::cout << "Input client's dogovor and new data to change" << std::endl;
	cin >> num;
	std::cout << "Clients fields " << '\n';
	std::cout << std::setw(20) << std::left << "Name:"
		<< std::setw(15) << std::left << "District:"
		<< std::setw(20) << std::left << "telephone:"
		<< std::setw(20) << std::left << "dogovor:"
		<< std::setw(20) << std::left << "data:"
		<< std::setw(20) << std::left << "price of install:"
		<< std::setw(25) << std::left << "price:"
		<< std::setw(25) << std::left << "data of pay:" << std::endl;

	std::cin >> client;
	data.change_person(num, client);
}

void Application2::run_find()
{
	std::cout << "Input file name where you want to save selected data" << std::endl;
	std::string path_result;

	std::cin >> path_result;

	std::cout << "MENU" << std::endl
		<< "1. Find by district" << std::endl
		<< "2. Find by date" << std::endl
		<< "3. Find by date_of_contract" << std::endl;

	char field[20];

	std::cin >> choice;

	switch (choice)
	{
	case 1:
		std::cout << "Input date: ";
		std::cin >> field;
		data.find_by_date(path_result, field);
		break;
	case 2:
		std::cout << "Input date of contract: ";
		std::cin >> field;
		data.find_by_date_of_contract(path_result, field);
		break;
	case 3:
		std::cout << "Input district: ";
		std::cin >> field;
		data.find_by_district(path_result, field);
		break;
	}

}

void Application2::run()
{

	std::cout << "Input clients in gym. ";



	std::cout << "MENU" << std::endl
		<< "1. Add" << std::endl
		<< "2. Erase" << std::endl
		<< "3. Change" << std::endl
		<< "4. Find" << std::endl
		<< "5. Insert" << std::endl
		<< "6. Transform Binary file to Txt file" << '\n'
		<< "Press another button to exit" << std::endl;

	bool key = true;
	std::string txt_path;
	std::string bin_path;

	while (key)
	{
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			run_add();
			break;
		case 2:
			run_erase();
			break;
		case 3:
			run_change();
			break;
		case 4:
			run_find();
			break;
		case 5:
			run_insert();
			break;
		case 6:
			std::cout << "Input bin file path" << std::endl;
			std::cin >> bin_path;
			std::cout << "Input txt file path" << std::endl;
			std::cin >> txt_path;
			from_bin_to_txt(bin_path, txt_path);
			break;
		default:
			key = false;
			break;


		}

	}
}