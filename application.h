#pragma once
#include<iostream>
#include<vector>
#include<iomanip>
#include<fstream>
#include "functions.h"
#include "function_2.h"

class Application1
{
protected:
	size_t choice;
	std::string path;
	std::vector<int> v;

	void run_fileModify();

	void run_CreateData();

	void run_LoadData();

	void run_Modify();

	void run_InputResult();


public:
	void run();
};

class Application2
{
protected:

	std::string path;

	size_t choice;

	Person_data data;

	void run_find();

	void run_change();

	void run_erase();

	void run_add();

	void run_insert();

public:
	Application2() = default;

	Application2(const Application2&) = default;

	~Application2() = default;

	explicit Application2(const std::string& path);

	void run();

};